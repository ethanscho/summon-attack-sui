module summon_attack::my_module {

    use std::string::{String};
    use sui::ecvrf;
    use sui::event;
    use std::debug;
    use std::hash;

    public struct RuleCard has key, store {
        id: UID,
        card_id: u32,
        drop_rate: u64
    }

    public struct RuleGame has key {
        id: UID,
        cards: vector<RuleCard>
    }

    public struct UserCard has key, store {
        id: UID,
        user_id: String,
        card_id: u32,
    }

    fun init(ctx: &mut TxContext) {
        // create rule game
        let rule_game = RuleGame {
            id: object::new(ctx),
            cards: vector[],
        };
        transfer::transfer(rule_game, ctx.sender());
    }

    public fun new_rule_card(
        rule_game: &mut RuleGame,
        card_id: u32,
        drop_rate: u64,
        ctx: &mut TxContext,
    ) {
        rule_game.cards.push_back(RuleCard {
            id: object::new(ctx),
            card_id: card_id,
            drop_rate: drop_rate,
        });
        debug::print(&rule_game.cards);
    }

    public fun user_card_id(self: &UserCard): u32 {
        self.card_id
    }

    /// Event on whether the output is verified
    public struct VerifiedEvent has copy, drop {
        is_verified: bool,
    }

    public fun new_user_card(
        rule_game: &mut RuleGame,
        output: vector<u8>, alpha_string: vector<u8>, public_key: vector<u8>, proof: vector<u8>, 
        user_id: String,
        ctx: &mut TxContext) {
        let verified = ecvrf::ecvrf_verify(&output, &alpha_string, &public_key, &proof);
        event::emit(VerifiedEvent {is_verified: verified});
        debug::print(&verified);

        // hash output and make random number in range 0~999
        let hash_output = hash::sha2_256(output);
        let num = ((hash_output[0] as u64) << 56) |
                  ((hash_output[1] as u64) << 48) |
                  ((hash_output[2] as u64) << 40) |
                  ((hash_output[3] as u64) << 32) |
                  ((hash_output[4] as u64) << 24) |
                  ((hash_output[5] as u64) << 16) |
                  ((hash_output[6] as u64) << 8) |
                  (hash_output[7] as u64);

        let random_num = num % 1000;
        debug::print(&random_num);

        // get corresponding card_id based on random number
        let mut i = 0;
        let mut n = 0;
        let mut card_id = 0;
        while (i < rule_game.cards.length()) {
            n = n + rule_game.cards[i].drop_rate;
            if (random_num < n) {
                card_id = rule_game.cards[i].card_id;
                break
            }
            else {
                i = i + 1;
            }
        };

        let user_card = UserCard {
            id: object::new(ctx),
            user_id: user_id,
            card_id: card_id,
        };
        transfer::transfer(user_card, ctx.sender());
    }

    #[test]
    fun test_verify_ecvrf_output() {
        let mut ctx = tx_context::dummy();

        let output: vector<u8> = vector[159, 158, 242, 33, 8, 134, 28, 227, 152, 137, 59, 63, 231, 161, 73, 236, 44, 39, 66, 123, 42, 212, 136, 39, 62, 213, 216, 6, 76, 20, 209, 94, 53, 27, 238, 93, 228, 29, 228, 176, 55, 144, 222, 176, 160, 33, 63, 198, 30, 70, 255, 187, 2, 5, 194, 195, 151, 114, 49, 14, 41, 146, 12, 4];
        let alpha_string: vector<u8> = vector[121, 101, 97, 104];
        let public_key: vector<u8> = vector[140,72,140,134,21,125,119,13,244,35,169,46,147,125,249,200,199,211,217,84,223,148,42,73,177,143,117,147,192,144,84,18];
        let proof: vector<u8> = vector[226, 121, 94, 107, 142, 143, 134, 2, 60, 145, 169, 90, 100, 111, 178, 73, 78, 145, 192, 215, 182, 220, 151, 248, 182, 149, 125, 235, 191, 71, 2, 9, 193, 79, 99, 117, 96, 27, 227, 132, 82, 188, 72, 180, 161, 231, 102, 206, 49, 245, 29, 246, 59, 16, 134, 71, 240, 38, 161, 230, 212, 134, 238, 95, 11, 195, 133, 182, 12, 90, 242, 186, 70, 124, 58, 145, 12, 139, 25, 3];

        let verified = ecvrf::ecvrf_verify(&output, &alpha_string, &public_key, &proof);
        debug::print(&verified);
    }

    #[test]
    fun test_game() {
        use sui::test_scenario;

        let admin = @0xAD;

        let mut scenario = test_scenario::begin(admin);
        {
            init(scenario.ctx());
        };

        scenario.next_tx(admin);
        {
            let mut rule_game = scenario.take_from_sender<RuleGame>();
            rule_game.new_rule_card(0, 500, scenario.ctx());
            rule_game.new_rule_card(1, 250, scenario.ctx());
            rule_game.new_rule_card(2, 250, scenario.ctx());
            scenario.return_to_sender(rule_game);
        };

        scenario.next_tx(admin);
        {
            //let mut game = scenario.take_from_sender<Game>();
            let mut rule_game = scenario.take_from_sender<RuleGame>();
            let output: vector<u8> = vector[159, 158, 242, 33, 8, 134, 28, 227, 152, 137, 59, 63, 231, 161, 73, 236, 44, 39, 66, 123, 42, 212, 136, 39, 62, 213, 216, 6, 76, 20, 209, 94, 53, 27, 238, 93, 228, 29, 228, 176, 55, 144, 222, 176, 160, 33, 63, 198, 30, 70, 255, 187, 2, 5, 194, 195, 151, 114, 49, 14, 41, 146, 12, 4];
            let alpha_string: vector<u8> = vector[121, 101, 97, 104];
            let public_key: vector<u8> = vector[140,72,140,134,21,125,119,13,244,35,169,46,147,125,249,200,199,211,217,84,223,148,42,73,177,143,117,147,192,144,84,18];
            let proof: vector<u8> = vector[226, 121, 94, 107, 142, 143, 134, 2, 60, 145, 169, 90, 100, 111, 178, 73, 78, 145, 192, 215, 182, 220, 151, 248, 182, 149, 125, 235, 191, 71, 2, 9, 193, 79, 99, 117, 96, 27, 227, 132, 82, 188, 72, 180, 161, 231, 102, 206, 49, 245, 29, 246, 59, 16, 134, 71, 240, 38, 161, 230, 212, 134, 238, 95, 11, 195, 133, 182, 12, 90, 242, 186, 70, 124, 58, 145, 12, 139, 25, 3];
            
            rule_game.new_user_card(output, alpha_string, public_key, proof, b"user_id".to_string(), scenario.ctx());
            scenario.return_to_sender(rule_game);
        };

        scenario.end();
    }
}