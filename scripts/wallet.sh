#!/bin/bash

# Publish the project and get the JSON response
json_response=$(sui client addresses --json)

# Use jq to parse the first address
first_address=$(echo "$json_response" | jq -r '.addresses[0][1]')

# Print the first address
echo "The first address is: $first_address"

export WALLET_ADDRESS=$first_address
echo "WALLET_ADDRESS: $WALLET_ADDRESS"