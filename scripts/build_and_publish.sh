#!/bin/bash

# Build the project
sui move build

# Publish the project and get the JSON response
json_response=$(sui client publish --gas-budget 50000000 --json)

# Parse the packageId from the JSON response using jq
package_id=$(echo "$json_response" | jq -r '.objectChanges[] | select(.type == "published") | .packageId')
gas_object_id=$(echo "$json_response" | jq -r '.effects.gasObject.reference.objectId')

# Parse the RuleGame objectId from the JSON response using jq
rule_game_object_id=$(echo "$json_response" | jq -r '.objectChanges[] | select(.objectType != null and (.objectType | contains("RuleGame"))) | .objectId')

# # Parse the my_module::Game objectId from the JSON response using jq
# game_object_id=$(echo "$json_response" | jq -r '.objectChanges[] | select(.objectType != null and (.objectType | contains("my_module::Game"))) | .objectId')

# Print the packageId
export CONTRACT_ADDRESS=$package_id
echo "CONTRACT_ADDRESS: $CONTRACT_ADDRESS"

export GAS_OBJECT_ID=$gas_object_id
echo "GAS_OBJECT_ID: $GAS_OBJECT_ID"

# export GAME_OBJECT_ID=$game_object_id
# echo "GAME_OBJECT_ID: $GAME_OBJECT_ID"

export RULE_GAME_OBJECT_ID=$rule_game_object_id
echo "RULE_GAME_OBJECT_ID: $RULE_GAME_OBJECT_ID"