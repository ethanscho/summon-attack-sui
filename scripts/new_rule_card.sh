#!/bin/bash

# Function to check if an environment variable exists and has a value
check_env_var() {
  var_name="$1"
  var_value="${!var_name}"

  if [ -z "$var_value" ]; then
    echo "Error: Environment variable $var_name is not set or is empty."
    exit 1
  fi
}

# Check required environment variables
check_env_var "WALLET_ADDRESS"
check_env_var "CONTRACT_ADDRESS"
check_env_var "GAS_OBJECT_ID"
check_env_var "RULE_GAME_OBJECT_ID"

# If the checks pass, continue with the rest of the script
echo "All required environment variables are set."

# Define variables
SUI_CLI="sui client"
MODULE_NAME="my_module"
FUNCTION_NAME="new_rule_card"

new_rule_card() {
  local card_id=$1
  local drop_rate=$2

  $SUI_CLI call --package $CONTRACT_ADDRESS \
      --module $MODULE_NAME \
      --function $FUNCTION_NAME \
      --args $RULE_GAME_OBJECT_ID $card_id $drop_rate \
      --gas-budget 20000000 \
      --gas $GAS_OBJECT_ID \
      --json
}

new_rule_card 0 70
new_rule_card 1 70
new_rule_card 2 70
new_rule_card 3 70
new_rule_card 4 15
new_rule_card 10 70
new_rule_card 11 70
new_rule_card 12 2
new_rule_card 13 34
new_rule_card 14 15
new_rule_card 17 34
new_rule_card 20 70
new_rule_card 21 34
new_rule_card 22 2
new_rule_card 23 34
new_rule_card 24 15
new_rule_card 25 2
new_rule_card 1000 34
new_rule_card 1001 70
new_rule_card 1003 70
new_rule_card 1004 34
new_rule_card 1006 70
new_rule_card 1007 34
new_rule_card 1010 8
new_rule_card 1020 8

