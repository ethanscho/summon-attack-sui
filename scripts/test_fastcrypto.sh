#!/bin/bash

check_env_var() {
  var_name="$1"
  var_value="${!var_name}"

  if [ -z "$var_value" ]; then
    echo "Error: Environment variable $var_name is not set or is empty."
    exit 1
  fi
}

# Check required environment variables
check_env_var "SECRET_KEY"

# Generate a random 8-digit integer
random_input=$(awk -v min=10000000 -v max=99999999 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
echo "Random Input: $random_input"

output=$(cargo run --manifest-path ./fastcrypto/Cargo.toml --bin ecvrf-cli prove --input $random_input --secret-key $SECRET_KEY)
# echo $output

proof=$(echo "$output" | awk '/Proof:/ {print $2}')
output_value=$(echo "$output" | awk '/Output:/ {print $2}')
echo "Proof: $proof"
echo "Output: $output_value"