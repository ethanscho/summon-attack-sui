#!/bin/bash

if [ -z "$1" ]; then
  echo "Error: User ID is not provided."
  exit 1
fi
USER_ID=$1

check_env_var() {
  var_name="$1"
  var_value="${!var_name}"

  if [ -z "$var_value" ]; then
    echo "Error: Environment variable $var_name is not set or is empty."
    exit 1
  fi
}

# Check required environment variables
check_env_var "SECRET_KEY"
check_env_var "PUBLIC_KEY"
check_env_var "WALLET_ADDRESS"
check_env_var "CONTRACT_ADDRESS"
check_env_var "GAS_OBJECT_ID"
check_env_var "RULE_GAME_OBJECT_ID"

# Generate a random 8-digit integer
random_input=$(awk -v min=10000000 -v max=99999999 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')

export RUSTFLAGS="-Awarnings"
output=$(cargo run --manifest-path ./fastcrypto/Cargo.toml --bin ecvrf-cli prove --input $random_input --secret-key $SECRET_KEY)

# Create proof and output using fastcrypto
proof=$(echo "$output" | awk '/Proof:/ {print $2}')
output_value=$(echo "$output" | awk '/Output:/ {print $2}')

# Convert proof and output to byte string
hex_to_byte_string() {
    local hex_string=$1
    local byte_string=""

    hex_to_decimal() {
        printf "%d" "0x$1"
    }

    # Loop through each pair of hex digits
    for (( i=0; i<${#hex_string}; i+=2 )); do
        # Get the hex pair
        hex_pair="${hex_string:$i:2}"
        # Convert hex to decimal
        decimal=$(hex_to_decimal "$hex_pair")
        # Append to the byte string
        byte_string+="$decimal,"
    done

    # Remove the trailing comma and space
    byte_string=${byte_string%,}

    echo "[$byte_string]"
}

SUI_CLI="sui client"
MODULE_NAME="my_module"           
FUNCTION_NAME="new_user_card"

PUBLIC_KEY_BYTES=$(hex_to_byte_string $PUBLIC_KEY)
ALPHA_STRING_BYTES=$(hex_to_byte_string $random_input)
OUTPUT_BYTES=$(hex_to_byte_string $output_value)
PROOF_BYTES=$(hex_to_byte_string $proof)

# Call the contract function
json_response=$($SUI_CLI call --package $CONTRACT_ADDRESS \
                --module $MODULE_NAME \
                --function $FUNCTION_NAME \
                --args $RULE_GAME_OBJECT_ID $OUTPUT_BYTES $ALPHA_STRING_BYTES $PUBLIC_KEY_BYTES $PROOF_BYTES $USER_ID \
                --gas-budget 20000000 \
                --gas $GAS_OBJECT_ID \
                --json)

user_card_object_id=$(echo "$json_response" | jq -r '.objectChanges[] | select(.objectType != null and (.objectType | contains("my_module::UserCard"))) | .objectId')

json_response=$($SUI_CLI object $user_card_object_id --json)
card_id=$(echo "$json_response" | jq '.content.fields.card_id')
card_object_id=$(echo "$json_response" | jq '.objectId')
echo $card_id $card_object_id