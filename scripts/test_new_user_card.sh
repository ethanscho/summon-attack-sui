#!/bin/bash

# Function to check if an environment variable exists and has a value
check_env_var() {
  var_name="$1"
  var_value="${!var_name}"

  if [ -z "$var_value" ]; then
    echo "Error: Environment variable $var_name is not set or is empty."
    exit 1
  fi
}

# Check required environment variables
check_env_var "WALLET_ADDRESS"
check_env_var "CONTRACT_ADDRESS"
check_env_var "GAS_OBJECT_ID"
check_env_var "GAME_OBJECT_ID"
check_env_var "RULE_GAME_OBJECT_ID"

# Define variables
SUI_CLI="sui client"
MODULE_NAME="my_module"           
FUNCTION_NAME="new_user_card"

# Define function arguments (vectors)
OUTPUT="159, 158, 242, 33, 8, 134, 28, 227, 152, 137, 59, 63, 231, 161, 73, 236, 44, 39, 66, 123, 42, 212, 136, 39, 62, 213, 216, 6, 76, 20, 209, 94, 53, 27, 238, 93, 228, 29, 228, 176, 55, 144, 222, 176, 160, 33, 63, 198, 30, 70, 255, 187, 2, 5, 194, 195, 151, 114, 49, 14, 41, 146, 12, 4"
ALPHA_STRING="121, 101, 97, 104"
PUBLIC_KEY="140,72,140,134,21,125,119,13,244,35,169,46,147,125,249,200,199,211,217,84,223,148,42,73,177,143,117,147,192,144,84,18"
PROOF="226, 121, 94, 107, 142, 143, 134, 2, 60, 145, 169, 90, 100, 111, 178, 73, 78, 145, 192, 215, 182, 220, 151, 248, 182, 149, 125, 235, 191, 71, 2, 9, 193, 79, 99, 117, 96, 27, 227, 132, 82, 188, 72, 180, 161, 231, 102, 206, 49, 245, 29, 246, 59, 16, 134, 71, 240, 38, 161, 230, 212, 134, 238, 95, 11, 195, 133, 182, 12, 90, 242, 186, 70, 124, 58, 145, 12, 139, 25, 3" 

# Remove spaces from the vector inputs
OUTPUT_CLEANED=$(echo $OUTPUT | tr -d ' ')
ALPHA_STRING_CLEANED=$(echo $ALPHA_STRING | tr -d ' ')
PUBLIC_KEY_CLEANED=$(echo $PUBLIC_KEY | tr -d ' ')
PROOF_CLEANED=$(echo $PROOF | tr -d ' ')

# Convert the cleaned vector inputs to the correct format
OUTPUT_FORMATTED="[$OUTPUT_CLEANED]"
ALPHA_STRING_FORMATTED="[$ALPHA_STRING_CLEANED]"
PUBLIC_KEY_FORMATTED="[$PUBLIC_KEY_CLEANED]"
PROOF_FORMATTED="[$PROOF_CLEANED]"

# Call the contract function
$SUI_CLI call --package $CONTRACT_ADDRESS \
    --module $MODULE_NAME \
    --function $FUNCTION_NAME \
    --args $GAME_OBJECT_ID $RULE_GAME_OBJECT_ID $OUTPUT_FORMATTED $ALPHA_STRING_FORMATTED $PUBLIC_KEY_FORMATTED $PROOF_FORMATTED \
    --gas-budget 20000000 \
    --gas $GAS_OBJECT_ID

# Check the result of the transaction
if [ $? -eq 0 ]; then
    echo "Transaction successful"
else
    echo "Transaction failed"
fi
