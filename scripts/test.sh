#!/bin/bash

# Function to convert hex string to comma-separated byte string
hex_to_byte_string() {
    local hex_string=$1
    local byte_string=""

    # Function to convert a hex pair to decimal
    hex_to_decimal() {
        printf "%d" "0x$1"
    }

    # Loop through each pair of hex digits
    for (( i=0; i<${#hex_string}; i+=2 )); do
        # Get the hex pair
        hex_pair="${hex_string:$i:2}"
        # Convert hex to decimal
        decimal=$(hex_to_decimal "$hex_pair")
        # Append to the byte string
        byte_string+="$decimal,"
    done

    # Remove the trailing comma and space
    byte_string=${byte_string%,}

    # Return the result
    echo "$byte_string"
}

# Example hex strings
PUBLIC_KEY="160eb4a5bbc91c96fd523ccbeea400363c055920ee791574d0406a2a56e2fa93691817085d96fced5a087405b642627701697d6f92436bf0bbeaa6d13ce7706f"
PROOF="abcdef1234567890abcdef1234567890abcdef1234567890abcdef1234567890"

# Convert them
PUBLIC_KEY_BYTES=$(hex_to_byte_string "$PUBLIC_KEY")
PROOF_BYTES=$(hex_to_byte_string "$PROOF")

# Print the results
echo "PUBLIC_KEY_BYTES=$PUBLIC_KEY_BYTES"
echo "PROOF_BYTES=$PROOF_BYTES"