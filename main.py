from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import subprocess

app = FastAPI()

class UserCard(BaseModel):
    user_id: str

@app.post("/user_card/")
def create_user_card(user_card: UserCard):
    try:
        result = subprocess.run(["./scripts/new_user_card.sh", user_card.user_id], capture_output=True, text=True)
        if result.returncode != 0:
            raise HTTPException(status_code=500, detail="Script execution failed")

        data = result.stdout.strip().split()
        card_id = int(data[0])
        object_id = data[1].strip('"')  # Remove the quotes if necessary
        return {"user_id": user_card.user_id, "card_id": card_id, "object_id": object_id}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8088)
