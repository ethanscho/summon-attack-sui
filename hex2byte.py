def hex_string_to_byte_list(hex_str):
    # 문자열을 2자씩 자릅니다.
    byte_list = [int(hex_str[i:i+2], 16) for i in range(0, len(hex_str), 2)]
    return byte_list

# 주어진 16진수 문자열
hex_str = "160eb4a5bbc91c96fd523ccbeea400363c055920ee791574d0406a2a56e2fa93691817085d96fced5a087405b642627701697d6f92436bf0bbeaa6d13ce7706f"

# 변환된 결과를 출력합니다.
byte_list = hex_string_to_byte_list(hex_str)

# remove space between comma
print(byte_list)